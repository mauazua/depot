class LineItem < ActiveRecord::Base
	belongs_to :order
	belongs_to :product
	belongs_to :cart

	#li = LineItem.find(...)
	#puts "This line item id for #{li.product.title}"

	#cart = Cart.find(...)
	#puts "This cart has #{cart.line_items.count} line items"
	def total_price
		product.price * quantity
	end
end
